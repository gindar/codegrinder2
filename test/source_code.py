#coding=utf-8
import pygame
import base
'''
@name Font 
'''

'''
    @class Font()
        @extends base.Dependency
'''
class Font(base.Dependency):
    def __init__(self):
        pass

    '''
        @method init(config)
        Init method
    '''
    def init(self, config):
        pass

    '''
        @method _getFont(fontpath, size)
            Init method
            @example
                testcall()
                if True:
                    pass
            @end_example
    '''
    def _getFont(self, fontpath, size):
        return font

    def addStyle(self, name, style, basedon="default"):
        pass

    def _render(self, font, text, color, shadow=None):
        pass
        return surface

    def renderString(self, text, style="default", cache=True):
        pass
        return surface

    def blitString(self, surface, position, text, style="default", cache=True):
        pass

    def renderText(self, text, style="default", width=200):
        pass
        return surface
