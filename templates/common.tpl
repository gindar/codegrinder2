@template:arg
<span class="arg"><span class="argtype">{type}</span><span class='argname'>{name}</span></span>

@template:argopt
<span class='arg' title='default value: {value}'><span class="argtype">{type}</span><span class='argname'>{name}</span><span class='argval'>={value}</span></span>

@template:section
<h2 id='{key}' class='type_section'>
    {name}
    <span class="anchor">
        <a href="#{key}" name="{key}">←</a>
        <a href="#">↑</a>
    </span>
</h2>
<div class='contentbox_section'>

@template:function
<h3 id='{key}' class='type_function {private}'>
    <span class="prefix">{prefix}</span><span class="name">{funcname}</span>(<span class="args">{args}</span>)
    <span class="anchor">
        <a href="#{key}" name="{key}">←</a>
        <a href="#">↑</a>
    </span>
</h3>
<div class='contentbox_function'>

@template:method
<h4 id='{key}' class='type_method {private}'>
    <span class="classname">{classname}.</span><span class="name">{funcname}</span>(<span class="args">{args}</span>)
    <span class="anchor">
        <a href="#{key}" name="{key}">←</a>
        <a href="#">↑</a>
    </span>
</h4>
<div class='contentbox_method'>

@template:class
<h3 id='{key}' class='type_class'>
    <span class='classname'>class </span>
    <span class="prefix">{prefix}</span><span class="name">{funcname}</span>(<span class="args">{args}</span>)
    <span class="anchor">
        <a href="#{key}" name="{key}">←</a>
        <a href="#">↑</a>
    </span>
</h3>
<div class='contentbox_class'>

@template:extends
<div class='classextends'>
    Extends: {extends}
</div>

@template:page
<!doctype html>
<meta http-equiv="pragma" content="no-cache"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>{pagetitle}</title>
<link href='http://fonts.googleapis.com/css?family=Source+Code+Pro:400,700|Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link href="cg2.css?{cgversion}_{version}" rel="stylesheet"/>
<div class="leftbox">
    <br/>
    <ul class="toc_global">
        {tocglobal}
    </ul>

    <hr/>
    <ul class="toc_local">
        {toclocal}
    </ul>
</div>

<h1>{pagetitle}</h1>
<div class="content">
    <div class="pagedescription">{pagedescription}</div>
    {content}
</div>
<div class="footer">
    CodeGrinder2 - {cgversion} by <a href="http://0x67.cz">Gindar</a>
</div>
<script>
    __highlighted = "";
    function update_hilit(){
        var name = location.hash.replace("#","");
        if( __highlighted != name ){
            clear_hilit(__highlighted);
            __highlighted = name;
            elm = document.getElementById( name );
            if( elm ){elm.className+=" selected"};

            elm = document.getElementById( 'link_'+name );
            if( elm ){
                elm.className+=" selected";
                parentNodesClassAdd(elm,"selpath");
            };
        };
    };
    function clear_hilit(name){
        elm = document.getElementById( name );
        if( elm ){elm.className=elm.className.replace(" selected",""); };
        elm = document.getElementById( "link_"+name );
        if( elm ){
            elm.className=elm.className.replace(" selected","");
            parentNodesClassRem(elm,"selpath");
        };
    };

    function parentNodesClassAdd(elm,cls){
        while(elm.parentNode){
            elm = elm.parentNode;
            if( elm.nodeName.toLowerCase() == "li" && elm.className.indexOf("fileline") == -1 ){
                elm.className += " "+cls;
            };
        };
    };

    function parentNodesClassRem(elm,cls){
        while(elm.parentNode){
            elm = elm.parentNode;
            if( elm.nodeName.toLowerCase() == "li" && elm.className.indexOf("fileline") == -1 ){
                elm.className = elm.className.replace(" "+cls,"");
            };
        };
    };

    setInterval(update_hilit,300);
    update_hilit();    
</script>
@template:css
<style>
body, html {
    font-size:12px;
    color:#000;
    background:#fff;
    font-family: "Open Sans", sans-serif;
    margin:0px;
    padding:0px;
}

pre {
    font-family: 'Source Code Pro', monospace;
}

/* Left menu box */
.leftbox {
    width:230px;
    background: #eee;
    color:#444;
    overflow:auto;
    position: fixed;
    left: 0px;
    top: 0px;
    height: 100%;
    border-right: solid #888 1px;
}

.leftbox h3 {
    font-size: 16px;
    padding: 15px 0px 3px 10px;
    margin: 0px;
}
.leftbox ul {
    list-style: none;
    margin: 0;
    padding: 3px;
    padding-left: 15px;
}


.leftbox .toc_group {
    font-weight: bold;
    display: block;
    border-bottom: solid #888 1px;
}

.leftbox ul.toc_local li ul {
   display: none;
}

.leftbox ul li.selected ul, .leftbox ul li.selpath ul {
    display: block;
}

.leftbox ul li.selected a {
    font-weight: bold;
}

.leftbox ul li.selected ul li a {
    font-weight: bold;
}

.leftbox ul li .filedesc {
    display: none;
}

.leftbox ul li .classname {
    font-size: 80%;
    color:#49c;
}

.leftbox ul li.toc_privatemethod,
.leftbox ul li.toc_privatefunction {
    opacity: 0.8;
    font-size: 90%;
}

.leftbox ul li.toc_section {
    margin-top: 5px;
    margin-bottom: 1px;
    white-space: nowrap;
}

.leftbox ul li.toc_section a {
    font-weight: bold;
    font-size: 120%;
}

.leftbox a { 
    font-weight:normal; 
    text-decoration:none; 
}

.leftbox a:hover { 
    font-weight:normal; 
    text-decoration:underline; 
}


/* Page content */
.content h2, 
.content h3,
.content h4 {
    padding: 0.4em 0.5em 0.4em 0.1em;
    margin: 1.2em 0em 0.2em 0em;
}

h1 {
    padding: 0px;
    padding-left: 20px;
    margin: 0;
    margin-top: 0.2em;
    margin-left: 230px;
    font-size:26px;
    border-bottom:solid #aaa 1px;
}

.content h2 {
    font-size: 22px;
}

.content h3 {
    font-size: 18px;
}

.content h4 {
    font-size: 16px;
}

.content {
    padding: 20px;
    padding-top: 5px;
    margin-left: 230px;
}

/* Function/class/method headers & boxes */
.content h3.type_class, 
.content h3.type_function, 
.content h4.type_method {
    font-family: 'Source Code Pro', monospace;
    font-weight: normal;
    color:#000;
}

.content h2.selected,
.content h3.selected,
.content h4.selected {
    background: #ffffaa;
}

.content h3.type_class .classname {
    font-weight:bold;
    color:#777;
    font-size: 85%;
}

.content .contentbox_class .classextends {
    font-weight: bold;
}

.content h3.type_class .name, 
.content h3.type_function .name, 
.content h4.type_method .name {
    font-weight:bold;
}

.content h3.type_class .prefix, 
.content h3.type_function .prefix, 
.content h4.type_method .classname {
    font-weight:normal;
    color:#333;
    font-size: 85%;
}

.content h4.private, 
.content h3.private {
    color: #555;
}

.contentbox_section {
}

.contentbox_class, 
.contentbox_method, 
.contentbox_function {
    margin-left: 10px;
}

.contentbox_class {
    margin-left: 30px;
}

.content .anchor a {
    float: right;
}

/* Heading anchor */
.content .anchor a {
    font-size: 110%;
    color:#fff;
    font-weight: bold;
    text-decoration:none;
}

.content h2:hover .anchor a,
.content h3:hover .anchor a,
.content h4:hover .anchor a {
    color:#406480;
    text-decoration:none;
}

.content h2 .anchor a:hover,
.content h3 .anchor a:hover,
.content h4 .anchor a:hover {
    color:#335066;
    text-decoration:none;
}
/* Function arguments */
.content h3.type_class .args,
.content h3.type_function .args, 
.content h4.type_method .args {
}

.content h3.type_class .args .arg,
.content h3.type_function .args .arg, 
.content h4.type_method .args .arg {
    font-size: 85%;
}

.content h3.type_class .args .argname,
.content h3.type_function .args .argname, 
.content h4.type_method .args .argname {
}

.content h3.type_class .args .argval,
.content h3.type_function .args .argval, 
.content h4.type_method .args .argval {
    display: none;
}

.content h3.type_class .args .argtype,
.content h3.type_function .args .argtype, 
.content h4.type_method .args .argtype {
    color:#aaa;
}

/* Content common */
a{ 
    color:#406480; 
    text-decoration:none; 
    font-weight:bold; 
}

a:hover { 
    color:#335066; 
    text-decoration:underline; 
}

/* Information boxes & example */
.content .example, 
.content .note, 
.content .warning { 
    margin: 5px 0px 5px 0px;
    font-family: 'Source Code Pro', monospace;
    font-size:15px;
    padding:4px 6px;
    border-radius: 2px;
}

.content .example {
    background:#f0f8ff; 
    border:solid #d9deff 1px; 
}

.content .note { 
    background:#fbfbfb; 
    border:dotted #ccc 1px;
}

.content .warning { 
    background:#ffded9; 
    border:solid #d28f85 1px;
}

.content .inlinecode {
    font-family: 'Source Code Pro', monospace;
    background: #f0f8ff;
    padding: 1px 3px;
}


/* Footer */
.footer  {
    padding: 10px;
    padding-left: 260px;
    border-top:solid #aaa 1px;
}


/* PYGMENTS */
.example pre { margin:0px; }
.example .hll { background-color: #ffffcc }
.example .c { color: #408080; font-style: italic } /* Comment */
.example .err { border-bottom: 1px dotted #FF0000 } /* Error */
.example .k { color: #008000; font-weight: bold } /* Keyword */
.example .o { color: #666666 } /* Operator */
.example .cm { color: #408080; font-style: italic } /* Comment.Multiline */
.example .cp { color: #BC7A00 } /* Comment.Preproc */
.example .c1 { color: #408080; font-style: italic } /* Comment.Single */
.example .cs { color: #408080; font-style: italic } /* Comment.Special */
.example .gd { color: #A00000 } /* Generic.Deleted */
.example .ge { font-style: italic } /* Generic.Emph */
.example .gr { color: #FF0000 } /* Generic.Error */
.example .gh { color: #000080; font-weight: bold } /* Generic.Heading */
.example .gi { color: #00A000 } /* Generic.Inserted */
.example .go { color: #808080 } /* Generic.Output */
.example .gp { color: #000080; font-weight: bold } /* Generic.Prompt */
.example .gs { font-weight: bold } /* Generic.Strong */
.example .gu { color: #800080; font-weight: bold } /* Generic.Subheading */
.example .gt { color: #0040D0 } /* Generic.Traceback */
.example .kc { color: #008000; font-weight: bold } /* Keyword.Constant */
.example .kd { color: #008000; font-weight: bold } /* Keyword.Declaration */
.example .kn { color: #008000; font-weight: bold } /* Keyword.Namespace */
.example .kp { color: #008000 } /* Keyword.Pseudo */
.example .kr { color: #008000; font-weight: bold } /* Keyword.Reserved */
.example .kt { color: #B00040 } /* Keyword.Type */
.example .m { color: #666666 } /* Literal.Number */
.example .s { color: #BA2121 } /* Literal.String */
.example .na { color: #7D9029 } /* Name.Attribute */
.example .nb { color: #008000 } /* Name.Builtin */
.example .nc { color: #0000aa; font-weight: bold } /* Name.Class */
.example .no { color: #880000 } /* Name.Constant */
.example .nd { color: #AA22FF } /* Name.Decorator */
.example .ni { color: #999999; font-weight: bold } /* Name.Entity */
.example .ne { color: #D2413A; font-weight: bold } /* Name.Exception */
.example .nf { color: #0000aa } /* Name.Function */
.example .nl { color: #A0A000 } /* Name.Label */
.example .nn { color: #0000FF; font-weight: bold } /* Name.Namespace */
.example .nt { color: #008000; font-weight: bold } /* Name.Tag */
.example .nv { color: #19177C } /* Name.Variable */
.example .ow { color: #AA6600; font-weight: bold } /* Operator.Word */
.example .w { color: #bbbbbb } /* Text.Whitespace */
.example .mf { color: #666666 } /* Literal.Number.Float */
.example .mh { color: #666666 } /* Literal.Number.Hex */
.example .mi { color: #666666 } /* Literal.Number.Integer */
.example .mo { color: #666666 } /* Literal.Number.Oct */
.example .sb { color: #BA2121 } /* Literal.String.Backtick */
.example .sc { color: #BA2121 } /* Literal.String.Char */
.example .sd { color: #BA2121; font-style: italic } /* Literal.String.Doc */
.example .s2 { color: #BA2121 } /* Literal.String.Double */
.example .se { color: #BB6622; font-weight: bold } /* Literal.String.Escape */
.example .sh { color: #BA2121 } /* Literal.String.Heredoc */
.example .si { color: #BB6688; font-weight: bold } /* Literal.String.Interpol */
.example .sx { color: #008000 } /* Literal.String.Other */
.example .sr { color: #BB6688 } /* Literal.String.Regex */
.example .s1 { color: #BA2121 } /* Literal.String.Single */
.example .ss { color: #19177C } /* Literal.String.Symbol */
.example .bp { color: #008000 } /* Name.Builtin.Pseudo */
.example .vc { color: #19177C } /* Name.Variable.Class */
.example .vg { color: #19177C } /* Name.Variable.Global */
.example .vi { color: #19177C } /* Name.Variable.Instance */
.example .il { color: #666666 } /* Literal.Number.Integer.Long */
</style>