import sys
import os
from distutils.core import setup

try:
    import pygments
except:
    print "Required: pygments (download at http://pygments.org)"
    raise SystemExit


def getVersion(fpath):
    version = "1.0"
    fp = open(fpath, "rb")
    for line in fp:
        if line.find("__version__") != -1:
            d = line.replace(" ", "").replace("\t", "").replace("\n", "").replace("\r", "").split("__version__=")
            version = d[1]
            break
    fp.close()
    return version
VERSION = getVersion("CodeGrinder2/__init__.py")
if sys.platform == "linux2":
    TEMPLATES_PATH = '/usr/share/CodeGrinder2/Templates'
else:
    TEMPLATES_PATH = os.path.expanduser('~/CodeGrinder2/Templates')

setup(
    name='CodeGrinder2',
    version=VERSION,
    packages=['CodeGrinder2'],
    data_files=[
        (TEMPLATES_PATH, ["templates/common.tpl"])
    ],
    scripts=[
        "cg2make",
        "cg2gen",
        "cg2diff"
    ]
)
