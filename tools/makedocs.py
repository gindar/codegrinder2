#!/usr/bin/python
'''
    CodeGrinder2 by Josef Vanzura
    http://zamraky.cz
'''
import json
import time
import glob
try:
    import CodeGrinder2
    from CodeGrinder2 import Parser, Generator
except:
    print "CodeGrinder2 is required. Download it from http://bitbucket.org/gindar/codegrinder2/ and install."
    raise SystemExit


def unicode2str_r(obj):
    if isinstance(obj, dict):
        result = {}
        for key in obj:
            result[str(key)] = unicode2str_r(obj[key])
        return result
    elif isinstance(obj, list):
        result = []
        for v in obj:
            result.append(unicode2str_r(v))
        return result
    elif isinstance(obj, unicode):
        return str(obj)
    else:
        return obj


fp = open("makedocs.json", "rb")
config = unicode2str_r(json.load(fp))
fp.close()


parser = Parser()
parser.verbose = True
generator = Generator(CodeGrinder2.__version__)
pages = []
for f in config["files"]:
    fnames = glob.glob(f)
    for ff in fnames:
        pages.append(parser.parseInlineCGD(ff, config["language"]))
if "docs" in config:
    for f in config["docs"]:
        pages.append(parser.parse(f, "cgd"))

version = time.strftime("%y%m%d")

generator.generate(
    pages,
    config["format"],
    outpath=config["outpath"],
    version=version
)
