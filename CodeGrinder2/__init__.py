
__version__ = "2.1.1"

from parser_base import Parser
from parser_js import ParserJavascript
from parser_py import ParserPython
from parser_cgd import ParserCGD

from generator_base import Generator
from generator_cgd import GeneratorCGD
from generator_html import GeneratorHTML

from diff import DiffFinder