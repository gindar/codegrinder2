from generator_cgd import GeneratorCGD
from generator_html import GeneratorHTML


class Generator:
    def __init__(self, cgversion):
        self.generators = [
            GeneratorCGD,
            GeneratorHTML
        ]
        self.cgversion = cgversion

    def generate(self, po_pages, format, **kwargs):
        options = {
            "outpath": ".",
            "version": "1.0"
        }
        options.update(kwargs)
        options["cgversion"] = self.cgversion

        gen = self._getGenerator(format)
        if not gen:
            raise Exception("Error: Generator not found (%s)" % format)

        for page in po_pages:
            _gen = gen(page, po_pages, **options)
            _gen.process()
            _gen.finalize()

    def generateIndex(self, outpath, code_version):
        gen = self._getGenerator("cgd")
        _gen = gen(None, None,
            outpath=outpath,
            cgversion=self.cgversion,
            version=code_version)
        _gen.makeIndex()

    def _getGenerator(self, format):
        format = format.lower()
        for gen in self.generators:
            if format in gen.formats:
                return gen
        return None

    def registerGenerator(self, generator):
        self.generators.append(generator)
