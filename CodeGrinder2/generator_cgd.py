import os
from cgobjects import *


class GeneratorCGD:
    formats = ["cgd", "codegrinder"]

    def __init__(self, po_page, po_pages, **kwargs):
        options = {
            "version": "1.0",
            "cgversion": "1.0",
            "outpath": ".",
            "template": ""
        }
        options.update(kwargs)
        
        self.page = po_page
        self.pages = po_pages
        self.outpath = options["outpath"]
        self.cgversion = options["cgversion"]
        self.code_version = options["version"]

        self.indent = "    "
        self.result = []

    def finalize(self):
        pass

    def _mkFunc(self, pre, name, args):
        if args:
            args = self._mkArgs(args)
            result = "%s %s(%s)" % (pre, name, args)
        else:
            result = "%s %s()" % (pre, name)

        self.result.append(result)

    def _mkArgs(self, args):
        result = []
        for name, value, typ in args:
            if typ:
                typ = "%s " % typ
            else:
                typ = ""
            name = "%s%s" % (typ,name)
            if value == None:
                result.append(name)
            else:
                arg = "%s=%s" % (name, value)
                result.append(arg)
        return ', '.join(result)

    def makeIndex(self):
        print "Generating: CGD index"
        self.result.append("@page index")
        self.result.append("@language cgd")
        self.result.append("@name (Project name)")
        self.result.append("@description No description")
        self.result.append("@section Files")
        self.result.append("<ul>{tocglobal}</ul>")

        self.save("index")

    def process(self):
        print "Generating: CGD"
        self.result.append("@cgversion %s" % self.cgversion)
        self.result.append("@version %s" % self.code_version)
        self.result.append("@page %s" % self.page.page)
        self.result.append("@language %s" % self.page.language)
        self.result.append("@name %s" % self.page.name)
        self.result.append("@description No description")
        self.result.append("@prefix %s" % self.page.prefix)

        for obj in self.page.content:
            self._processObj(obj)

        self.save(self.page.page)

    def save(self, fname):
        result = '\n'.join(self.result)

        path = os.path.join(self.outpath, fname)
        path = "%s.cgd" % path
        fp = open(path, "wb")
        fp.write(result)
        fp.close()
        print "Saved to %s" % path

    def _processObj(self, obj):
        if isinstance(obj, PO_Class):
            pre = "\n\n@class"
            self._mkFunc(pre, obj.name, obj.args)
            if obj.extends:
                extends = ', '.join(obj.extends)
                self.result.append("%s@extends %s" % (self.indent, extends))

            self.result.append("%s@nodoc" % self.indent)

            for method in obj.methods:
                self._processObj(method)
        elif isinstance(obj, PO_Method):
            pre = "\n@method"
            self._mkFunc(pre, obj.name, obj.args)
            self.result.append("%s@nodoc" % self.indent)

        elif isinstance(obj, PO_Function):
            pre = "\n\n@function"
            self._mkFunc(pre, obj.name, obj.args)
            self.result.append("%s@nodoc" % self.indent)


