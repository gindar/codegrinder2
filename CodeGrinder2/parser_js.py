import re
import os
from cgobjects import *


class ParserJavascript:
    language_names = ["js", "javascript", "jscript"]
    suffixes = ["js"]
    inline_avail = True

    def __init__(self, filename, indent_str):
        self.language = "javascript"
        self.page = PO_Page(filename, self.language)
        self.indent_str = indent_str

        module = '.'.join(os.path.basename(filename).split(".")[:-1])
        self.setPage(module)
        self.page.prefix = ""

        self.context_class = None
        self.context_last_function = None

    def setPage(self, page):
        self.page.prefix = page
        self.page.name = page
        self.page.page = page

    def _getIndent(self, line, txline):
        index = line.index(txline)
        return line[:index]

    def inlineStart(self, line):
        _line = line.replace(self.indent_str, "", 1).strip()
        result = _line.startswith("/**")
        try:
            indent = self._getIndent(line, _line)
        except:
            indent = ""
            pass
        return (result, indent)

    def inlineEnd(self, line):
        line = line.replace(self.indent_str, "").strip()
        return line.startswith("*/") or line.startswith("**/")

    def _parseArgs(self, line):
        line = '('.join(line.split("(")[1:])
        line = ')'.join(line.split(")")[:-1])
        args = line
        if not args:
            return []
        args = args.split(",")

        _args = []
        for a in args:
            a = a.strip()
            _args.append((a, None, None))

        return _args

    def processClass(self, line):
        name, extends = self._parseFunc(line)
        inst = self.page.addClass(lineno, name)
        if extends:
            for e in extends:
                inst.extends.append(e[0])
        self.context_class = inst

    def transformFunctionToClass(self, func):
        print self.page.content
        index = self.page.content.index(func)
        inst = PO_Class(self.page, func.lineno, func.name)
        inst.prefix = func.prefix
        inst.private = func.private
        self.page.content[index] = inst
        self.context_class = inst

    def processLine(self, line, lineno):
        print line
        if line.startswith("/") or line.startswith(" "):
            return 0
        if "JAX.makeClass" in line:
            # is JAX class
            name = line.split("=")[0].strip()
            inst = self.page.addClass(lineno, name)
            args = self._parseArgs(line.split("=")[1].strip())
            if len(args) > 1:
                inst.extends.append(args[1])
            self.context_class = inst

        elif ".prototype." in line and "function" in line:
            # is method
            line = line.replace("{", "").replace("function", "").strip()
            name = line.split(".prototype.")[1].split("=")[0].strip()
            args = line.split("=")[1].strip()
            args = self._parseArgs(args)
            clsn = line.split(".prototype.")[0]

            if not self.context_class or self.context_class.name != clsn:
                if not self.context_last_function:
                    raise Exception("Cannot find constructor of [%s].%s" % (clsn, name))
                self.transformFunctionToClass(self.context_last_function)

            if "$constructor" in line:
                # is constructor
                self.context_class.args = args
            else:
                inst = self.page.addMethod(lineno, name, self.context_class)
                inst.args = args

        elif "function" in line:
            # is function
            if "=" in line:
                # xxx = function()
                name = line.split("=")[0].strip()
            else:
                # function xxx()
                name = re.sub(r"function ([a-zA-Z0-9_\$]+)(\\\()?(.*)", r"\1", line)
            args = self._parseArgs(line)
            inst = self.page.addFunction(lineno, name)
            inst.args = args
            self.context_last_function = inst
