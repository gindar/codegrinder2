from cgobjects import PO_Class, PO_Method, PO_Function


class DiffFinder:
    def __init__(self, source, cgd):
        self.source = source
        self.cgd = cgd

        self.messages = []

        self.source_dict = {}
        self.cgd_dict = {}

    def _addObject(self, obj, target_dict):
        if isinstance(obj, PO_Method):
            target_dict["%s.%s.%s" % (obj.pclass.prefix, obj.pclass.name, obj.name)] = obj
            target_dict["%s.%s" % (obj.pclass.name, obj.name)] = obj
        else:
            target_dict["%s.%s" % (obj.prefix, obj.name)] = obj
        target_dict[obj.name] = obj

    def _addObjects(self, objlist, target_dict):
        for obj in objlist:
            if isinstance(obj, PO_Class):
                self._addObject(obj, target_dict)
                self._addObjects(obj.methods, target_dict)
            elif isinstance(obj, PO_Method):
                self._addObject(obj, target_dict)
            elif isinstance(obj, PO_Function):
                self._addObject(obj, target_dict)

    def _objectDiff(self, sobj, dobj):
        if sobj.t != dobj.t:
            self.message("Different object type", sobj, dobj, False, "type")

        if sobj.args != dobj.args:
            if len(sobj.args) != len(dobj.args):
                self.message("Different arguments count", sobj, dobj, True, "args")
            else:
                self.message("Different arguments", sobj, dobj, True, "args")

    def processDiff(self):
        self._addObjects(self.source.content, self.source_dict)
        self._addObjects(self.cgd.content, self.cgd_dict)

        notfound = []
        for key in self.source_dict:
            source_obj = self.source_dict[key]
            if not key in self.cgd_dict:
                # object not found in CGD
                if not source_obj in notfound:
                    notfound.append(source_obj)
            else:
                if source_obj in notfound:
                    notfound.remove(source_obj)
                cgd_obj = self.cgd_dict[key]
                self._objectDiff(source_obj, cgd_obj)

        for obj in notfound:
            self.message("Not documented", obj, None, True, "name")

        notfound = []
        for key in self.cgd_dict:
            cgd_obj = self.cgd_dict[key]
            if not key in self.source_dict:
                # object not found in Source
                if not cgd_obj in notfound:
                    notfound.append(cgd_obj)
            else:
                if cgd_obj in notfound:
                    notfound.remove(cgd_obj)

        for obj in notfound:
            self.message("Not found in source", obj, None, True, "name")

        for msg in self.messages:
            print msg

    def message(self, msg, sobj, dobj=None, showargs=False, hilit="", subinfo=""):
        st = self._warnString(msg)
        if subinfo:
            st += "\n    %s" % subinfo
        if sobj:
            st += "\n  src %s" % self._objDesc(sobj, showargs, hilit)
        if dobj:
            st += "\n  doc %s" % self._objDesc(dobj, showargs, hilit)
        if st not in self.messages:
            self.messages.append(st)
        return st

    def _objDesc(self, obj, showargs, hilit=""):
        lineno = obj.lineno
        name = obj.name
        if "name" in hilit:
            name = self._hilitString(name)

        if isinstance(obj, PO_Class):
            typ = "class"
            name = "%s.%s" % (obj.prefix, name)
        elif isinstance(obj, PO_Method):
            typ = "method"
            name = "%s.%s.%s" % (obj.pclass.prefix, obj.pclass.name, name)
        elif isinstance(obj, PO_Function):
            typ = "function"
            name = "%s.%s" % (obj.prefix, name)

        if "typ" in hilit:
            typ = self._hilitString(typ)

        args = ""
        if showargs:
            args = "(%s)" % self._mkArgs(obj.args)

        if "args" in hilit:
            args = self._hilitString(args)

        msg = "%4s: %s %s %s" % (lineno, typ, name, args)
        return msg

    def _warnString(self, st):
        return "\033[1;31m%s\033[0m" % st

    def _hilitString(self, st):
        return "\033[1;33m%s\033[0m" % st

    def _mkArgs(self, args):
        result = []
        for name, value, typ in args:
            if typ:
                typ = "%s " % typ
            else:
                typ = ""
            name = "%s%s" % (typ, name)
            if value == None:
                result.append(name)
            else:
                arg = "%s=%s" % (name, value)
                result.append(arg)
        return ', '.join(result)
