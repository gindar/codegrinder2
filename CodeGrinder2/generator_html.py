import sys
import os
import re
import unicodedata
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter

from cgobjects import *


class GeneratorHTML:
    formats = ["html"]

    def __init__(self, po_page, po_pages, **kwargs):
        options = {
            "version": "1.0",
            "cgversion": "1.0",
            "outpath": ".",
            "template": ""
        }
        options.update(kwargs)

        template = None
        if not options["template"]:
            tpl = self.searchTemplate("common.tpl")
            if tpl:
                print "Using default template: %s" % tpl
                template = tpl
            else:
                raise Exception("Default template not found!")
        elif os.path.exists(options["template"]):
            print "Using template: %s" % options["template"]
            template = options["template"]
        else:
            tpl = self.searchTemplate(options["template"])
            if tpl:
                print "Using template: %s" % tpl
                template = tpl
            else:
                raise Exception("Template '%s' not found!" % options["template"])

        self.page = po_page
        self.pages = po_pages
        self.outpath = options["outpath"]
        self.content = []
        self.links = {}
        self.template = Template(template)
        self.attrs = {
            "cgversion": "%s" % options["cgversion"],
            "version": "%s" % options["version"],
            "pagetitle": "",
            "pagedescription": "",
            "toclocal": [],
            "tocglobal": [],
        }

        self.genLinkList()
        self.genPagesList()

    def searchTemplate(self, name):
        base_paths = [
            os.path.abspath("."),
            os.path.abspath(os.path.dirname(__file__) + "/.."),
            os.path.expanduser("~/CodeGrinder2"),
            "/usr/share/CodeGrinder2"
        ]
        subdirs = ("templates", "cg2templates", "Templates", "")

        testpaths = []

        for bp in base_paths:
            for sd in subdirs:
                testpaths.append(os.path.join(bp, sd))

        for path in testpaths:
            pth = os.path.join(path, name)
            if os.path.exists(pth):
                return pth
        return None

    def finalize(self):
        data = self.template.parts["css"]
        data = data.replace("<style>", "").replace("</style>", "")
        path = os.path.join(self.outpath, "cg2.css")
        print "Writing CSS: %s" % path
        fp = open(path, "wb")
        fp.write(data)
        fp.close()

    def genPagesList(self):
        links = self.attrs["tocglobal"]
        pages = []
        pages_obj = {}
        groups = {}
        groups_list = []
        indexpage = None
        i = 0
        for p in self.pages:
            if p.page == "index":
                indexpage = p
                continue
            pages_obj[p.name] = p
            if not p.group in groups:
                groups_list.append(p.group)
                groups[p.group] = []
            groups[p.group].append(p)
            pages.append(p.name)
            i += 1
        pages.sort()
        if indexpage:
            selected = ""
            if self.page == indexpage:
                selected = "selected"
            links.append("<li class='toc_module %s isindex'><a href='index.html'>Index</a></li>" % selected)

        print groups_list
        for grp in groups_list:
            if grp != "":
                links.append("<li><span class='toc_group'>%s</span> <ul>" % grp)
            for p in groups[grp]:
                selected = ""
                if self.page.page == p.page:
                    selected = "selected"
                links.append("<li class='toc_module %s'><a href='%s.html'>%s</a><span class='filedesc'> - %s</span></li>" % (
                            selected, p.page, p.name, p.description))
            if grp != "":
                links.append("</ul></li>")

    def genLinkList(self):
        for p in self.pages:
            for o in p.content:
                self._mkObjPaths(o)

    def _mkObjPaths(self, obj):
        keys = []
        if isinstance(obj, PO_Class) or isinstance(obj, PO_Function):
            keys.append("%s.%s" % (obj.prefix, obj.name))
            keys.append("%s" % (obj.name))

        if isinstance(obj, PO_Method):
            keys.append("%s.%s.%s" % (obj.pclass.prefix, obj.prefix, obj.name))
            keys.append("%s.%s" % (obj.prefix, obj.name))
            keys.append("%s" % (obj.name))

        if isinstance(obj, PO_Section):
            keys.append("%s" % (obj.name))

        if isinstance(obj, PO_Class):
            for o in obj.methods:
                self._mkObjPaths(o)

        if isinstance(obj, PO_Page):
            for o in obj.content:
                self._mkObjPaths(o)

        for key in keys:
            self.links[key] = obj
            gkey = "%s.%s" % (obj.page, key)
            self.links[gkey] = obj
            gkey = "%s:%s" % (obj.page, key)
            self.links[gkey] = obj

    def processLinks(self, st):
        if "[" in st:
            for link in self.links:
                k = "[%s]" % link
                o = self.links[link]
                l = self.getLink(o, True)
                r = "<a href='%s'>%s</a>" % (l, o.name)
                st = st.replace(k, r)
        return st

    def processText(self, text):
        text = self.processLinks(text)
        matches = re.findall(r"(\*[a-zA-Z0-9\. _:-]+\*)", text)
        for m in matches:
            tx = "<span class='inlinecode'>%s</span>" % m[1:-1]
            text = text.replace(m, tx)
        return text

    def addToc(self, link, title, typ, close=True, ttext=""):
        page = "%s.html" % self.page.page
        st = "<li id='link_%s' class='toc_%s'>%s<a href='%s#%s'>%s</a>" % (
            link, typ, ttext, page, link, title)
        if close:
            st += "</li>"
        self.addTocRaw(st)

    def addTocRaw(self, st):
        self.attrs["toclocal"].append(st)

    def htmlsafe(self, st):
        return st.replace("<", "&lt;").replace(">", "&gt;")

    def makeExample(self, example):
        text = []
        for tx in example.content:
            text.append(tx.text)
        text = '\n'.join(text)

        lexer = get_lexer_by_name(example.language, stripall=False)
        formatter = HtmlFormatter(linenos=False, cssclass="example", encoding='utf-8')
        result = highlight(unicode(text, "utf-8"), lexer, formatter)
        return result

    def getLink(self, obj, full=False):
        if isinstance(obj, PO_Class) or isinstance(obj, PO_Function):
            v = "%s.%s" % (obj.prefix, obj.name)
        elif isinstance(obj, PO_Method):
            v = "%s.%s.%s" % (obj.pclass.prefix, obj.prefix, obj.name)
        elif isinstance(obj, PO_Section):
            v = obj.name
        else:
            return ""

        v = self.makeLinkKey(v)
        if full:
            v = "%s.html#%s" % (obj.page.page, v)
        return v

    def makeLinkKey(self, unistr):
        unistr = unistr.lower().strip()
        for c in ("?", '"', "'", "/", "\\", " ", "#", ":", "."):
            unistr = unistr.replace(c, "_")
        if not isinstance(unistr, unicode):
            unistr = unicode(unistr, "utf-8")
        result = "".join(ch for ch in unicodedata.normalize(
            "NFD", unistr) if not unicodedata.combining(ch))
        return result.encode('ascii', 'ignore')

    def makeArgs(self, args):
        result = []
        opt = 0
        index = 0
        for name, value, typ in args:
            if not typ:
                typ = ""
            else:
                typ = typ + " "
            if value is None:
                v = self.template.processTemplate("arg", {"name": name, "type": typ})
                if index != 0:
                    v = ", " + v
                result.append(v)
            else:
                opt += 1
                v = self.template.processTemplate("argopt", {
                    "name": name, "value": value, "type": typ})
                if index != 0:
                    arg = " [, %s" % v
                else:
                    arg = "[%s" % v
                result.append(arg)
            index += 1
        result = ''.join(result)
        if opt:
            result += "]" * opt
        return result

    def makeExtends(self, obj_extends):
        extends = []
        for e in obj_extends:
            extends.append("[%s]" % e)
        extends = self.processLinks(', '.join(extends))
        extends = self.template.processTemplate("extends", {
            "extends": extends})
        return extends

    def process(self):
        for obj in self.page.content:
            self._processObj(obj)

        self.attrs["pagetitle"] = self.page.name
        self.attrs["pagedescription"] = self.page.description
        self.attrs["toclocal"] = ''.join(self.attrs["toclocal"])
        self.attrs["tocglobal"] = ''.join(self.attrs["tocglobal"])

        tpldata = self.template.processTemplate("page", {
            "content": ''.join(self.content)
        })
        tpldata = self.template.processData(tpldata, self.attrs)

        path = os.path.join(self.outpath, self.page.page)
        path = "%s.html" % path
        fp = open(path, "wb")
        fp.write(tpldata)
        fp.close()
        print "Saved to %s" % path

    def _processObj(self, obj):
        if isinstance(obj, PO_Class):
            link = self.getLink(obj)
            prefix = obj.prefix
            if prefix:
                prefix = "%s." % prefix
            args = self.makeArgs(obj.args)

            tpldata = self.template.processTemplate("class", {
                "key": link,
                "prefix": prefix,
                "funcname": obj.name,
                "args": args
            })

            self.addToc(link, obj.name, "class", False, "<span class='classname'>class</span> ")
            self.addTocRaw("<ul>")

            self.content.append(tpldata)

            if obj.extends:
                extends = self.makeExtends(obj.extends)
                self.content.append(extends)

            for o in obj.content:
                self._processObj(o)

            for method in obj.methods:
                self._processObj(method)

            self.content.append("</div>")

            self.addTocRaw("</ul></li>")

        elif isinstance(obj, PO_Method):
            link = self.getLink(obj)
            args = self.makeArgs(obj.args)

            tpldata = self.template.processTemplate("method", {
                "key": link,
                "classname": obj.pclass.name,
                "funcname": obj.name,
                "args": args,
                "private": obj.private and "private" or ""
            })

            if obj.private:
                self.addToc(link, obj.name, "privatemethod")
            else:
                self.addToc(link, obj.name, "method")

            self.content.append(tpldata)
            for o in obj.content:
                self._processObj(o)
            self.content.append("</div>")

        elif isinstance(obj, PO_Function):
            link = self.getLink(obj)
            prefix = obj.prefix
            if prefix:
                prefix = "%s." % prefix
            args = self.makeArgs(obj.args)

            tpldata = self.template.processTemplate("function", {
                "key": link,
                "prefix": prefix,
                "funcname": obj.name,
                "args": args,
                "private": obj.private and "private" or ""
            })

            if obj.private:
                self.addToc(link, obj.name, "privatefunction")
            else:
                self.addToc(link, obj.name, "function")

            self.content.append(tpldata)
            for o in obj.content:
                self._processObj(o)
            self.content.append("</div>")

        elif isinstance(obj, PO_Section):
            link = self.getLink(obj)

            tpldata = self.template.processTemplate("section", {
                "key": link,
                "name": obj.name
            })
            self.addToc(link, obj.name, "section")

            self.content.append(tpldata)
            for o in obj.content:
                self._processObj(o)
            self.content.append("</div>")

        elif isinstance(obj, PO_Example):
            text = self.makeExample(obj)
            self.content.append(text)

        elif isinstance(obj, PO_Text):
            text = obj.text
            text = self.processText(text)
            self.content.append(text)

        elif isinstance(obj, PO_Tag):
            deftag = self.page.deftags[obj.tagid]
            index = len(self.content)
            for o in deftag.content:
                self._processObj(o)

            while index < len(self.content):
                d = self.content[index]
                argindex = 0
                for arg in deftag.args:
                    d = d.replace("{%s}" % arg, obj.args[argindex])
                    argindex += 1
                self.content[index] = d
                index += 1

        elif isinstance(obj, PO_Note):
            text = obj.text
            text = self.processText(text)
            self.content.append("<div class='note'>%s</div>" % text)

        elif isinstance(obj, PO_Warning):
            text = obj.text
            text = self.processText(text)
            self.content.append("<div class='warning'>%s</div>" % text)


class Template:
    def __init__(self, fname):
        self.fname = fname
        self.parts = {}
        self.actual = ""
        self.parse()

    def processTemplate(self, name, values):
        st = self.parts[name]
        return self.processData(st, values)

    def processData(self, st, values):
        for p in values:
            st = st.replace("{%s}" % p, values[p])
        return st

    def parse(self):
        data = open(self.fname, "rb").read()
        data = data.split("\n")
        for line in data:
            if line.startswith("@template:"):
                self.actual = line.replace("@template:", "").strip()
                self.parts[self.actual] = ""
            elif self.actual:
                self.parts[self.actual] += line

