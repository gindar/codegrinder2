from parser_cgd import ParserCGD
from parser_js import ParserJavascript
from parser_py import ParserPython
from parser_c import ParserC


class Parser:
    def __init__(self):
        self.cgd_parser = ParserCGD
        self.parsers = [
            ParserCGD,
            ParserJavascript,
            ParserPython,
            ParserC
        ]
        self.indent_str = "    "

    def parse(self, filename, language=None):
        print "Parsing file: %s" % filename
        parser = self._detectParser(filename, language)
        if not parser:
            raise Exception("Error: Language parser not found.")

        print "Load file: %s" % filename
        fp = open(filename, "rb")
        data = fp.read().replace("\r", "\n").replace("\n\n", "\n")
        data = data.split("\n")
        fp.close()

        parser = parser(filename, self.indent_str)
        print "Language: %s" % parser.language

        lineno = 1
        for line in data:
            line = line.replace("\t", self.indent_str)
            line = line.replace("\n", "")
            parser.processLine(line, lineno)
            lineno += 1

        return parser.page

    def _detectParser(self, filename, language):
        if language:
            for parser in self.parsers:
                if language.lower() in parser.language_names:
                    return parser

        suffix = filename.split(".")[-1]
        for parser in self.parsers:
            if suffix.lower() in parser.suffixes:
                return parser
        return None

    def registerParser(self, cls):
        self.parsers.append(cls)

    def parseInlineCGD(self, filename, language=None):
        print "Extracting CGD from file: %s" % filename
        parser = self._detectParser(filename, language)
        if not parser:
            raise Exception("Error: Language parser not found.")

        print "Load file: %s" % filename
        fp = open(filename, "rb")
        data = fp.read().replace("\r", "\n").replace("\n\n", "\n")
        data = data.split("\n")
        fp.close()

        parser = parser(filename, self.indent_str)
        print "Language: %s" % parser.language

        lineno = 1
        result = []
        result.append("@language %s" % parser.language)
        parsing = False
        indent = ""
        for line in data:
            line = line.replace("\t", self.indent_str)
            line = line.replace("\n", "")
            start_result = parser.inlineStart(line)
            if parsing:
                if parser.inlineEnd(line):
                    parsing = False
                else:
                    line = line.replace(indent + self.indent_str, "", 1)
                    result.append(line)
            elif start_result[0]:
                parsing = True
                indent = start_result[1]
            lineno += 1

        _parser = parser
        parser = self.cgd_parser(filename, self.indent_str)
        parser.setPage(_parser.page.page)

        lineno = 1
        for line in result:
            parser.processLine(line, lineno)
            lineno += 1

        return parser.page
