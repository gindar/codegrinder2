class ProtoContent:
    def addContent(self, obj):
        if isinstance(obj, PO_Example):
            self.content.append(obj)
            obj.parent = self
        else:
            self.content.append(obj)


class PO_Section(ProtoContent):
    def __init__(self, page, lineno, name):
        self.lineno = lineno
        self.name = name
        self.page = page
        self.content = []


class PO_Example(ProtoContent):
    def __init__(self, lineno, language):
        self.lineno = lineno
        self.language = language
        self.parent = None
        self.indent = 0
        self.content = []


class PO_Deftag(ProtoContent):
    def __init__(self, lineno, name):
        self.lineno = lineno
        self.name = name
        self.parent = None
        self.args = []
        self.indent = 0
        self.content = []


class PO_Note:
    def __init__(self, lineno, text):
        self.lineno = lineno
        self.text = text


class PO_Warning:
    def __init__(self, lineno, text):
        self.lineno = lineno
        self.text = text


class PO_Text:
    def __init__(self, lineno, text):
        self.lineno = lineno
        self.text = text


class PO_Tag:
    def __init__(self, lineno, tagid):
        self.lineno = lineno
        self.tagid = tagid
        self.args = []


class PO_Class(ProtoContent):
    def __init__(self, page, lineno, name):
        self.t = "class"
        self.page = page
        self.lineno = lineno
        self.name = name
        self.prefix = ""
        self.extends = []
        self.args = []
        self.methods = []
        self.content = []

    def addMethod(self, method):
        method.pclass = self
        method.prefix = self.name
        method.page = self.page
        self.methods.append(method)


class PO_Method(ProtoContent):
    def __init__(self, lineno, name):
        self.t = "method"
        self.lineno = lineno
        self.name = name
        self.page = None
        self.prefix = ""
        self.pclass = None
        self.private = False
        self.args = []
        self.content = []


class PO_Function(ProtoContent):
    def __init__(self, page, lineno, name):
        self.t = "function"
        self.lineno = lineno
        self.name = name
        self.page = page
        self.prefix = ""
        self.private = False
        self.args = []
        self.content = []


class PO_Page(ProtoContent):
    def __init__(self, filename, language):
        self.page = ""
        self.name = ""
        self.prefix = ""
        self.group = ""
        self.description = ""
        self.filename = filename
        self.language = language
        self.content = []
        self.deftags = {}

    def addClass(self, lineno, name):
        inst = PO_Class(self, lineno, name)
        inst.prefix = self.prefix
        self.content.append(inst)
        return inst

    def addMethod(self, lineno, name, pclass):
        inst = PO_Method(lineno, name)
        pclass.addMethod(inst)
        return inst

    def addFunction(self, lineno, name):
        inst = PO_Function(self, lineno, name)
        inst.prefix = self.prefix
        self.content.append(inst)
        return inst
