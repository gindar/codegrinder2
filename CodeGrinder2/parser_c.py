import re
import os
from cgobjects import *


class ParserC:
    language_names = ["c", "c++", "cpp"]
    suffixes = ["cpp", "c", "h", "hpp"]
    inline_avail = True

    def __init__(self, filename, indent_str):
        self.language = "c"
        self.page = PO_Page(filename, self.language)
        self.indent_str = indent_str

        module = '.'.join(os.path.basename(filename).split(".")[:-1])
        self.setPage(module)
        self.page.prefix = ""

        self.context_class = None
        self.context_last_function = None

    def setPage(self, page):
        self.page.prefix = page
        self.page.name = page
        self.page.page = page

    def _getIndent(self, line, txline):
        index = line.index(txline)
        return line[:index]

    def inlineStart(self, line):
        _line = line.replace(self.indent_str, "", 1).strip()
        result = _line.startswith("/**")
        try:
            indent = self._getIndent(line, _line)
        except:
            indent = ""
            pass
        return (result, indent)

    def inlineEnd(self, line):
        line = line.replace(self.indent_str, "").strip()
        return line.startswith("*/") or line.startswith("**/")

    def _parseArgs(self, line):
        line = '('.join(line.split("(")[1:])
        line = ')'.join(line.split(")")[:-1])
        args = line
        if not args:
            return []
        args = args.split(",")

        _args = []
        for a in args:
            a = a.strip()
            _args.append((a, None, None))

        return _args

    def processLine(self, line, lineno):
        return None
