import re
import os
from cgobjects import *


class ParserPython:
    language_names = ["py", "python"]
    suffixes = ["py"]
    inline_avail = True

    def __init__(self, filename, indent_str):
        self.language = "python"
        self.page = PO_Page(filename, self.language)
        self.indent_str = indent_str

        module = '.'.join(os.path.basename(filename).split(".")[:-1])
        self.setPage(module)

        self.context_class = None

    def setPage(self, page):
        self.page.prefix = page
        self.page.name = page
        self.page.page = page

    def _getIndent(self, line, txline):
        index = line.index(txline)
        return line[:index]

    def inlineStart(self, line):
        _line = line.strip()
        result = _line.startswith("'''")
        indent = self._getIndent(line, _line)
        return (result, indent)

    def inlineEnd(self, line):
        _line = line.strip()
        result = _line.startswith("'''")
        return result

    def _parseFunc(self, line):
        name = re.sub(r"([a-zA-Z0-9_\$]+)(\\\()?(.*)", r"\1", line)
        args = []
        if "(" in line:
            line = '('.join(line.split("(")[1:])
            line = ')'.join(line.split(")")[:-1])
            args = self._parseArgs(line)
        return (name, args)

    def _parseArgs(self, args):
        if not args:
            return []
        usepchar = False
        for c in ["'", '"', "[", "(", "{"]:
            if c in args:
                usepchar = True
                break

        if usepchar:
            args = self._parseArgsByChar(args)
        else:
            args = args.split(",")

        _args = []
        for a in args:
            if "=" in a:
                aname = a.split("=")[0].strip()
                atype = None
                if " " in aname:
                    atype = aname.split(" ")[0]
                    aname = aname.split(" ")[1]
                aval = '='.join(a.split("=")[1:])
                _args.append((aname, aval, atype))
            else:
                a = a.strip()
                atype = None
                if " " in a:
                    atype = a.split(" ")[0]
                    a = a.split(" ")[1]
                _args.append((a, None, atype))

        return _args

    def _parseArgsByChar(self, _args):
        args = [""]
        mode = ""
        modec = ""
        for c in _args:
            if mode == "" and c == ",":
                # new argument
                args.append("")
                continue

            if mode == "s" and c == modec:
                mode = ""
            elif mode == "" and c in ("'", '"'):
                mode = "s"
                modec = c

            if mode == "a" and c == modec:
                mode = ""
            elif mode == "" and c in ("(", "[", "{"):
                mode = "a"
                if c == "(":
                    modec = ")"
                if c == "[":
                    modec = "]"
                if c == "{":
                    modec = "}"

            args[-1] += c
        return args

    def processLine(self, line, lineno):
        if line.startswith("class") and line.find(":") != -1:
            # is class
            line = line.replace("class ", "")
            name, extends = self._parseFunc(line)
            inst = self.page.addClass(lineno, name)
            if extends:
                for e in extends:
                    inst.extends.append(e[0])
            self.context_class = inst

        elif line.startswith(self.indent_str + "def") and line.find(":") != -1:
            # is method
            line = line.replace(self.indent_str + "def ", "")
            name, args = self._parseFunc(line)
            args = args[1:]
            if name == "__init__":
                self.context_class.args = args
            else:
                inst = self.page.addMethod(lineno, name, self.context_class)
                inst.args = args

        elif line.startswith("def") and line.find(":") != -1:
            # is function
            line = line.replace("def ", "")
            name, args = self._parseFunc(line)
            inst = self.page.addFunction(lineno, name)
            inst.args = args
