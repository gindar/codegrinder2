import re
from cgobjects import *
from parser_py import ParserPython


class ParserCGD(ParserPython):
    language_names = ["cgd", "codegrinder"]
    suffixes = ["cgd"]
    inline_avail = False

    def __init__(self, filename, indent_str):
        self.language = "cgd"
        self.page = PO_Page(filename, self.language)
        self.indent_str = indent_str

        self.context_class = None
        self.context = self.page

    def setPage(self, page):
        self.page.prefix = page
        self.page.name = page
        self.page.page = page

    def contextReset(self):
        self.context.pop()

    def _parseFunc(self, line):
        name = re.sub(r"([a-zA-Z0-9_\$\.]+)(\\\()?(.*)", r"\1", line)
        args = []
        if "(" in line:
            line = '('.join(line.split("(")[1:])
            line = ')'.join(line.split(")")[:-1])
            args = self._parseArgs(line)
        return (name, args)

    def _parseAttrs(self, st):
        if len(st) > 0:
            st = st.strip()
            return st
        else:
            return ""

    def processLine(self, line, lineno):
        indentnum = line.count(self.indent_str)

        if line.startswith("@class"):
            # is class
            line = line.replace("@class ", "")
            name, args = self._parseFunc(line)
            inst = self.page.addClass(lineno, name)
            inst.args = args
            self.context_class = inst
            self.context = inst

        elif line.startswith("@method"):
            # is method
            line = line.replace("@method ", "")
            name, args = self._parseFunc(line)
            inst = self.page.addMethod(lineno, name, self.context_class)
            inst.args = args
            self.context = inst

        elif line.startswith("@function"):
            # is function
            line = line.replace("@function ", "")
            name, args = self._parseFunc(line)
            inst = self.page.addFunction(lineno, name)
            inst.args = args
            self.context = inst

        elif line.startswith("@section"):
            line = line.replace("@section", "").strip()
            inst = PO_Section(self.page, lineno, line)
            self.page.addContent(inst)
            self.context = inst

        # Object attributes
        elif line.startswith(self.indent_str + "@extends"):
            line = line.replace(self.indent_str + "@extends", "")
            v = self._parseAttrs(line)
            if v:
                v = v.strip().split(",")
                self.context_class.extends = v

        elif line.startswith(self.indent_str + "@private"):
            line = line.replace(self.indent_str + "@private", "")
            self.context.private = True

        # Page info
        elif line.startswith("@cgversion"):
            pass
        elif line.startswith("@version"):
            pass
        elif line.startswith("@prefix"):
            v = self._parseAttrs(line.replace("@prefix", ""))
            self.page.prefix = v

        elif line.startswith("@group"):
            v = self._parseAttrs(line.replace("@group", ""))
            print "GROUP", v
            self.page.group = v

        elif line.startswith("@language"):
            v = self._parseAttrs(line.replace("@language", ""))
            self.page.language = v

        elif line.startswith("@name"):
            v = self._parseAttrs(line.replace("@name", ""))
            self.page.name = v

        elif line.startswith("@page"):
            v = self._parseAttrs(line.replace("@page", ""))
            self.page.page = v

        elif line.startswith("@description"):
            v = self._parseAttrs(line.replace("@description", ""))
            self.page.description = v

        # Object content
        elif line.strip().startswith("@tag"):
            attrs = self._parseAttrs(line.replace("@tag", "")).split(" ")
            name = attrs[0]
            if not name in self.page.deftags:
                raise Exception("Tag '%s' not found." % name)
            obj = PO_Tag(lineno, name)
            obj.args = attrs[1:]
            self.context.addContent(obj)

        elif line.strip().startswith("@note"):
            line = self._parseAttrs(line.replace("@note", ""))
            if line:
                obj = PO_Note(lineno, line)
                self.context.addContent(obj)

        elif line.strip().startswith("@warning"):
            line = self._parseAttrs(line.replace("@warning", ""))
            if line:
                obj = PO_Warning(lineno, line)
                self.context.addContent(obj)

        elif line.strip().startswith("@example"):
            # example
            language = self._parseAttrs(line.replace("@example", ""))
            if language == "":
                language = self.page.language
            obj = PO_Example(lineno, language)
            self.context.addContent(obj)
            obj.parent = self.context
            obj.indent = indentnum
            self.context = obj

        elif line.strip().startswith("@end_example"):
            if isinstance(self.context, PO_Example):
                self.context = self.context.parent

        elif line.strip().startswith("@deftag"):
            # example
            attrs = self._parseAttrs(line.replace("@deftag", "")).split(" ")
            name = attrs[0]
            obj = PO_Deftag(lineno, name)
            obj.parent = self.context
            obj.indent = indentnum
            obj.args = attrs[1:]
            self.page.deftags[name] = obj
            self.context = obj

        elif line.strip().startswith("@end_deftag"):
            if isinstance(self.context, PO_Deftag):
                self.context = self.context.parent

        else:
            # plain text & example text
            if isinstance(self.context, PO_Example) or isinstance(self.context, PO_Deftag):
                cnt = (self.context.indent + 1) * len(self.indent_str)
                line = line[cnt:]
            obj = PO_Text(lineno, line)
            self.context.addContent(obj)
