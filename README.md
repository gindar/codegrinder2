codegrinder2
============

Simple manual/handbook/documentation maker with BAD documentation.

##Requirements
 * Python 2.5+
 * Pygments ( http://pygments.org/ )

##Installation (Linux/MacOS)
1. Install Pygments
    
    http://pygments.org/download/

2. Clone CodeGrinder2 repository

```
    git clone https://github.com/gindar/codegrinder2.git
    cd codegrinder2
    su -c python setup.py install
```

## Supported languages

 * Python (classes, functions, methods)
 * JavaScript (JAX classes, methods, functions)

## Usage

1. Generate CGD documents from source (cg2gen)
    
```
    cg2gen emperor.py --outpath=doc/
```

2. Write documentation

```
    @function DestroyCity(city_name, only_people=False)
        This function is really useful.
```

3. Make HTML files from CGD and template

```
    cg2make doc/emperor.cgd --outpath=doc/
```

4. Enjoy!

## CGD syntax

```
@function thisIsFunction(argument, optional_argument="defaul_value")
    Function description.

    @example
        thisIsFunction(1, 2)
    @end_example

    @note This is note box.
    @warning This is warning box.

@class ThisIsClass(constructor_argument)
    @extends ParentClass

    Class description.

@method ThisIsMethod()
    Description of method.


```

## Inline parsing
Example of inline docs
```
'''
    @class TestClass(arg1, arg2=None)
        @extends ParentClass
'''
class TestClass(ParentClass):
    def __init__(self, arg1, arg2=None):
        pass
    
    '''
        @method doSomething(arg1)
            Description of method
    '''
    def doSomething(self, arg1):
        pass

```

Make docs
```
cg2make source_code.py --inline=python
```

## TODO

 * js-parser: JAK classes
 * js-parser: native classes
 * inline parser
